﻿using DB;
using System.Collections.Generic;
using DB.Users;
using System.Linq;

namespace DAL
{
    public static class GetData
    {
        public static List<Product> GetAllProducts()
        {
            var res = Product.listOfProducts;

            return res.ToList();
        }

        public static List<Order> GetAllOrders()
        {
            var res = Order.ordersList;

            return res.ToList();
        }

        public static List<Guest> GetAllUsers()
        {
            var res = Guest.people;

            return res.ToList();
        }
    }
}
