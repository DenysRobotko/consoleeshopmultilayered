﻿using DB;
using DB.Users;
using DAL;
using System;
using System.Linq;
using System.Collections.Generic;

namespace BLL
{
    public class GuestAction : IBaseAction
    {
        public Product FindProductByName(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            Product element = GetData.GetAllProducts().FirstOrDefault(x => x.probuctName == name);
            Console.WriteLine($"Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
            return element;
        }

        public void ShowProducts()
        {
            int i = 1;
            foreach (var element in GetData.GetAllProducts())
            {
                Console.WriteLine($"{i}. Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
                i++;
            }
        }

        public void SignIn(string name, string password, int number)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");

            Guest g = new Guest();
            switch (number)
            {
                case 1:
                    g = new User(name, password);
                    break;
                case 2:
                    g = new Administrator(name, password);
                    break;
                default:
                    Console.WriteLine("You have entered wrong number");
                    break;
            }
            if (Guest.people.Any(x => x.Nickname == name))
                Console.WriteLine("You have already signed in. Try to log in");
            else
            {
                Guest.people.Add(g);
                Console.WriteLine("Person added");
            }
        }

        public void LogIn(string name, string password)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");
            if (Guest.people.Any(x => x.Nickname != name || x.Password != password))
                Console.WriteLine("There is no such user");
        }
    }
}
