﻿using DAL;
using DB;
using DB.Users;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL
{
    public class UserAction : User, IBaseAction
    {
        public UserAction(string n, string p):base(n, p)
        {

        }
        public void NewOrder(int number, int choose)
        {
            if (number == 0)
                throw new ArgumentNullException(nameof(number), "Parametr was null");
            if (number > 0 && number < GetData.GetAllProducts().Count + 1)
            {
                Order newOrder = new Order(this , GetData.GetAllProducts()[number - 1]);
                switch (choose)
                {
                    case 1:
                        newOrder.status = OrderStatus.New;
                        Order.ordersList.Add(newOrder);
                        break;
                    case 2:
                        newOrder.status = OrderStatus.Canceled;
                        Order.ordersList.Add(newOrder);
                        break;
                    default:
                        newOrder.status = OrderStatus.Canceled;
                        Order.ordersList.Add(newOrder);
                        Console.WriteLine("You have canceled your order");
                        break;
                }
            }
            else
            {
                var ex = new IndexOutOfRangeException(nameof(number) + " was out of range");
                throw ex;
            }
        }

        public void SetOrderStatusReceived()
        {
            Order order = GetData.GetAllOrders().FindLast(x => x.buyer == this);
            Console.WriteLine("Have you received your order? y/n");
            string str = Console.ReadLine();
            if (str == "y")
            {
                order.status = OrderStatus.Received;
                Console.WriteLine("Status has changed");
            }
            else if (str != "y")
                Console.WriteLine("You have not changed order status");
        }

        public void ShowHistory()
        {
            if (this.Nickname == null)
            {
                Console.WriteLine("Log in to watch your history");
            }
            else
            {
                var resultedList = from orders in GetData.GetAllOrders()
                                   where orders.buyer == this
                                   select orders;
                foreach (var element in resultedList)
                {
                    Console.WriteLine($"{element.buyer.Nickname} Ordered {element.product.probuctName}. Now satus is {element.status}");
                }
            }
        }

        public void ChangePersonalData(string newName, string newPassword)
        {
            Nickname = newName ?? throw new ArgumentNullException(nameof(newName), "Parametr was null");
            this.Password = newPassword ?? throw new ArgumentNullException(nameof(newPassword), "Parametr was null");
        }

        public void SignOut()
        {
            this.Nickname = null;
            this.Password = null;
        }

        public Product FindProductByName(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (GetData.GetAllProducts().Any(x => x.probuctName != name))
            {
                var ex = new NullReferenceException();
                throw ex;
            }
            Product element = GetData.GetAllProducts().FirstOrDefault(x => x.probuctName == name);
            Console.WriteLine($"1. Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
            return element;
        }

        public void ShowProducts()
        {
            int i = 1;
            foreach (var element in GetData.GetAllProducts())
            {
                Console.WriteLine($"{i}. Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
                i++;
            }
        }

        public void SignIn(string name, string password, int number)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");

            Guest g = new Guest();
            switch (number)
            {
                case 1:
                    g = new User(name, password);
                    break;
                case 2:
                    g = new Administrator(name, password);
                    break;
                default:
                    Console.WriteLine("You have entered wrong number");
                    break;
            }
            if (GetData.GetAllUsers().Any(x => x.Nickname == name))
                Console.WriteLine("You have already signed in. Try to log in");
            else
            {
                people.Add(g);
                Console.WriteLine("Person added");
            }
        }

        public void LogIn(string name, string password)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");
            if (GetData.GetAllUsers().Any(x => x.Nickname != name || x.Password != password))
                Console.WriteLine("There is no such user");
        }
    }
}
