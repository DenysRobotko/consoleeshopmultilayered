﻿using DAL;
using DB;
using DB.Users;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL
{
    public class AdministratorAction : Administrator, IBaseAction
    {
        public AdministratorAction(string n, string p) : base(n, p)
        {

        }
        public Product FindProductByName(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (GetData.GetAllProducts().Any(x => x.probuctName != name))
            {
                var ex = new NullReferenceException();
                throw ex;
            }
            Product element = GetData.GetAllProducts().FirstOrDefault(x => x.probuctName == name);
            Console.WriteLine($"Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
            return element;
        }

        public void ShowProducts()
        {
            int i = 1;
            foreach (var element in GetData.GetAllProducts())
            {
                Console.WriteLine($"{i}. Product name: {element.probuctName}\t category: {element.productCategory}" +
                    $"\t description {element.productDescription}\t price: {element.productCost}");
                i++;
            }
        }

        public void SignIn(string name, string password, int number)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");

            Guest g = new Guest();
            switch (number)
            {
                case 1:
                    g = new User(name, password);
                    break;
                case 2:
                    g = new Administrator(name, password);
                    break;
                default:
                    Console.WriteLine("You have entered wrong number");
                    break;
            }
            if (GetData.GetAllUsers().Any(x => x.Nickname == name))
                Console.WriteLine("You have already signed in. Try to log in");
            else
            {
                people.Add(g);
                Console.WriteLine("Person added");
            }
        }

        public void LogIn(string name, string password)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            else if (password == null)
                throw new ArgumentNullException(nameof(password), "Parametr was null");
            if (GetData.GetAllUsers().Any(x => x.Nickname != name || x.Password != password))
                Console.WriteLine("There is no such user");
        }

        public void NewOrder(int number, int choose)
        {
            if (number == 0)
                throw new ArgumentNullException(nameof(number), "Parametr was null");
            Console.WriteLine("Select which product you want to chose:");
            if (number > 0 && number < GetData.GetAllProducts().Count + 1)
            {
                Order newOrder = new Order(this, Product.listOfProducts[number - 1]);
                switch (choose)
                {
                    case 1:
                        newOrder.status = OrderStatus.New;
                        Order.ordersList.Add(newOrder);
                        break;
                    case 2:
                        newOrder.status = OrderStatus.Canceled;
                        Order.ordersList.Add(newOrder);
                        break;
                    default:
                        newOrder.status = OrderStatus.Canceled;
                        Order.ordersList.Add(newOrder);
                        Console.WriteLine("You have canceled your order");
                        break;
                }
            }
            else
            {
                var ex = new IndexOutOfRangeException(nameof(number) + " was out of range");
                throw ex;
            }
        }

        public void ShowUsers()
        {
            int i = 1;
            foreach (var element in GetData.GetAllUsers())
                if (element is User)
                {
                    Console.WriteLine($"{i}. " + element.Nickname);
                    i++;
                }
        }

        public void ChangeUsersData(string newName)
        {
            if (newName == null)
                throw new ArgumentNullException(nameof(newName), "Parametr was null");
            ShowUsers();
            Console.WriteLine("Enter new name");
            string personName = Console.ReadLine();
            Guest guest = GetData.GetAllOrders().Find(x => x.buyer.Nickname == personName).buyer;
            if (guest != null)
            {
                guest.Nickname = newName;
            }
        }

        public void AddProduct(string name, string category, string description, double cost)
        {
            if (name == null || category == null || description == null || cost == 0)
                throw new ArgumentNullException(nameof(name));
            if (GetData.GetAllProducts().Any(x => x.probuctName == name))
                throw new ArgumentException(nameof(name) + " already exists");
            Product.listOfProducts.Add(new Product(name, category, description, cost));
        }

        public void ChangeProductInformation(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Parametr was null");
            ShowProducts();
            if (GetData.GetAllProducts().Any(x => x.probuctName == name))
            {
                Console.WriteLine("Enter new category");
                this.FindProductByName(name).productCategory = Console.ReadLine();
                Console.WriteLine("Enter new description");
                this.FindProductByName(name).productDescription = Console.ReadLine();
                Console.WriteLine("Enter new price");
                double.TryParse(Console.ReadLine(), out double number);
                FindProductByName(name).productCost = number;
            }
        }

        public void ShowOrders()
        {
            foreach (var order in GetData.GetAllOrders())
                Console.WriteLine($"{order.buyer.Nickname} ordered {order.product.probuctName} with status {order.status}");
        }

        public void ShowOrderStatus()
        {
            int i = 0;
            foreach (var elem in Enum.GetNames(typeof(OrderStatus)))
            {
                i++;
                Console.WriteLine(i + ". " + elem);
            }
        }

        public void ChangeOrderStatus(string personName, int num)
        {
            if (personName == null)
                throw new ArgumentNullException(nameof(personName), "Parametr was null");
            Order singleOrder =GetData.GetAllOrders().Find(x => x.buyer.Nickname == personName);

            if (num > 0 && num < Enum.GetNames(typeof(OrderStatus)).Length + 1)
            {
                singleOrder.status = (OrderStatus)Enum.GetValues(typeof(OrderStatus)).GetValue(num - 1);
                Console.WriteLine("Status has changed");
            }
            else
            {
                var ex = new IndexOutOfRangeException(nameof(num) + " was out of range");
                throw ex;
            }
        }

        public void SignOut()
        {
            this.Nickname = null;
            this.Password = null;
        }
    }
}
