﻿using DB;

namespace BLL
{
    interface IBaseAction
    {
        public Product FindProductByName(string name);
        public void ShowProducts();
        public void SignIn(string name, string password, int number);
        public void LogIn(string name, string password);
    }
}
