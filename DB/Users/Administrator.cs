﻿using DB;
using System;
using System.Linq;

namespace DB.Users
{
    public class Administrator : Guest
    {
        public Administrator(string name, string password)
        {
            Nickname = name;
            Password = password;
        }

        //public void NewOrder(int number, int choose)
        //{
        //    if (number == 0)
        //        throw new ArgumentNullException(nameof(number), "Parametr was null");
        //    Console.WriteLine("Select which product you want to chose:");
        //    if (number > 0 && number < Product.listOfProducts.Count + 1)
        //    {
        //        Order newOrder = new Order(this, Product.listOfProducts[number - 1]);
        //        switch (choose)
        //        {
        //            case 1:
        //                newOrder.status = OrderStatus.New;
        //                Order.ordersList.Add(newOrder);
        //                break;
        //            case 2:
        //                newOrder.status = OrderStatus.Canceled;
        //                Order.ordersList.Add(newOrder);
        //                break;
        //            default:
        //                newOrder.status = OrderStatus.Canceled;
        //                Order.ordersList.Add(newOrder);
        //                Console.WriteLine("You have canceled your order");
        //                break;
        //        }
        //    }
        //    else
        //    {
        //        var ex = new IndexOutOfRangeException(nameof(number) + " was out of range");
        //        throw ex;
        //    }
        //}

        //public void ShowUsers()
        //{
        //    foreach (var element in Guest.people)
        //        if (element is User)
        //        {
        //            Console.WriteLine("1. " + element.Nickname);
        //        }
        //}

        //public void ChangeUsersData(string newName)
        //{
        //    if (newName == null)
        //        throw new ArgumentNullException(nameof(newName), "Parametr was null");
        //    ShowUsers();
        //    Console.WriteLine("Enter new name");
        //    string personName = Console.ReadLine();
        //    Guest guest = Order.ordersList.Find(x => x.buyer.Nickname == personName).buyer;
        //    if (guest != null)
        //    {
        //        guest.Nickname = newName;
        //    }
        //}

        //public void AddProduct(string name, string category, string description, double cost)
        //{
        //    if (name == null || category == null || description == null || cost == 0)
        //        throw new ArgumentNullException(nameof(name));
        //    if (Product.listOfProducts.Any(x => x.probuctName == name))
        //        throw new ArgumentException(nameof(name) + " already exists");
        //    Product.listOfProducts.Add(new Product(name, category, description, cost));
        //}

        //public void ChangeProductInformation(string name)
        //{
        //    if (name == null)
        //        throw new ArgumentNullException(nameof(name), "Parametr was null");
        //    ShowProducts();
        //    if (Product.listOfProducts.Any(x => x.probuctName == name))
        //    {
        //        Console.WriteLine("Enter new category");
        //        this.FindProductByName(name).productCategory = Console.ReadLine();
        //        Console.WriteLine("Enter new description");
        //        this.FindProductByName(name).productDescription = Console.ReadLine();
        //        Console.WriteLine("Enter new price");
        //        double.TryParse(Console.ReadLine(), out double number);
        //        FindProductByName(name).productCost = number;
        //    }
        //}

        //public void ShowOrders()
        //{
        //    foreach (var order in Order.ordersList)
        //        Console.WriteLine($"{order.buyer.Nickname} ordered {order.product.probuctName} with status {order.status}");
        //}

        //public void ShowOrderStatus()
        //{
        //    int i = 0;
        //    foreach (var elem in Enum.GetNames(typeof(OrderStatus)))
        //    {
        //        i++;
        //        Console.WriteLine(i + ". " + elem);
        //    }
        //}

        //public void ChangeOrderStatus(string personName, int num)
        //{
        //    if (personName == null)
        //        throw new ArgumentNullException(nameof(personName), "Parametr was null");
        //    Order singleOrder = Order.ordersList.Find(x => x.buyer.Nickname == personName);

        //    if (num > 0 && num < Enum.GetNames(typeof(OrderStatus)).Length + 1)
        //    {
        //        singleOrder.status = (OrderStatus)Enum.GetValues(typeof(OrderStatus)).GetValue(num - 1);
        //        Console.WriteLine("Status has changed");
        //    }
        //    else
        //    {
        //        var ex = new IndexOutOfRangeException(nameof(num) + " was out of range");
        //        throw ex;
        //    }
        //}

        //public void SignOut()
        //{
        //    this.Nickname = null;
        //    this.Password = null;
        //}
    }
}
