﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB
{
    public class Product
    {
        public string probuctName { get; set; }
        public string productCategory { get; set; }
        public string productDescription { get; set; }
        public double productCost { get; set; }

        public static List<Product> listOfProducts = new List<Product>()
        {new Product("Galaxy s20", "Phone", "New flagship", 1200)};

        public Product(string name, string category, string description, double cost)
        {
            probuctName = name;
            productCategory = category;
            productDescription = description;
            productCost = cost;
        }
        public Product()
        {

        }
    }
}
