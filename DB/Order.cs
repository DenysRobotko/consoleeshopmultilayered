﻿using DB.Users;
using System.Collections.Generic;

namespace DB
{
    public class Order
    {
        public Guest buyer { get; set; }
        public Product product { get; set; }
        public OrderStatus status { get; set; }
        public static List<Order> ordersList = new List<Order>();
        public Order(Guest buyer, Product product)
        {
            this.buyer = buyer;
            this.product = product;
        }

        public Order()
        {

        }
    }
}
