﻿using System;

namespace ConsoleEShopMultilayered
{
    public static class Program
    {
        static void Main()
        {
            UIL.Menu menu = new UIL.Menu();
            menu.MenuInterraction();
            GC.KeepAlive(DB.Product.listOfProducts);
            GC.KeepAlive(DB.Order.ordersList);
            GC.KeepAlive(DB.Users.Guest.people);
            
        }
    }
}
