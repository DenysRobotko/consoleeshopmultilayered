﻿using BLL;
using System;
namespace UIL
{
    public class Interraction
    {
        public void GuestInterraction()
        {
            Console.WriteLine("Hello, as a guest you can do a few things:\n" +
                                "1. Show all products.\n" +
                                "2. Search product by its name.\n" +
                                "3. Sign in as a new customer.\n" +
                                "4. Log in to your account.");
            bool temp1 = true;
            while (temp1)
            {
                int.TryParse(Console.ReadLine(), out int guestNumber);
                GuestAction guest = new GuestAction();
                switch (guestNumber)
                {
                    case 1:
                        guest.ShowProducts();
                        break;
                    case 2:
                        Console.WriteLine("Enter product name, you want to search.");
                        string productToSearch = Console.ReadLine();
                        guest.FindProductByName(productToSearch);
                        break;
                    case 3:
                        Console.WriteLine("Enter you nickname:");
                        string newNickname = Console.ReadLine();
                        Console.WriteLine("Enter your password:");
                        string newPassword = Console.ReadLine();
                        Console.WriteLine("If you want to sign in as a user press 1, if as a administrator press 2");
                        int.TryParse(Console.ReadLine(), out int numberChoose);
                        guest.SignIn(newNickname, newPassword, numberChoose);
                        break;
                    case 4:
                        Console.WriteLine("Enter you nickname:");
                        string nickname = Console.ReadLine();
                        Console.WriteLine("Enter your password:");
                        string password = Console.ReadLine();
                        guest.LogIn(nickname, password);
                        break;
                    default:
                        Console.WriteLine("You have entered wrong number.");
                        Console.Clear();
                        temp1 = false;
                        break;
                }
            }
        }

        public void UserInterraction()
        {
            Console.WriteLine("Hello, as a user you can do a few things:\n" +
                                "1. Show all products.\n" +
                                "2. Search product by its name.\n" +
                                "3. Create new order and ordering or declining order.\n" +
                                "4. Show orders history and orders status.\n" +
                                "5. Shange order status to received.\n" +
                                "6. Change personal information.\n" +
                                "7. Sign out.");

            Console.WriteLine("Enter you nickname:");
            string newNickname = Console.ReadLine();
            Console.WriteLine("Enter your password:");
            string newPassword = Console.ReadLine();
            UserAction user = new UserAction(newNickname, newPassword);
            bool temp1 = true;
            while (temp1)
            {
                int.TryParse(Console.ReadLine(), out int userNumber);
                switch (userNumber)
                {
                    case 1:
                        user.ShowProducts();
                        break;
                    case 2:
                        Console.WriteLine("Enter product name, you want to search.");
                        string productToSearch = Console.ReadLine();
                        user.FindProductByName(productToSearch);
                        break;
                    case 3:

                        Console.WriteLine("Enter product number you want to buy.");
                        user.ShowProducts();
                        int.TryParse(Console.ReadLine(), out int numberToBuy);
                        Console.WriteLine("Do you want to confirm your order? \n Yes - 1, No - 2");
                        int.TryParse(Console.ReadLine(), out int choose);
                        user.NewOrder(numberToBuy, choose);
                        break;
                    case 4:
                        user.ShowHistory();
                        break;
                    case 5:
                        user.SetOrderStatusReceived();
                        break;
                    case 6:
                        Console.WriteLine("Enter new nickname:");
                        string nickname = Console.ReadLine();
                        Console.WriteLine("Enter new password:");
                        string password = Console.ReadLine();
                        user.ChangePersonalData(nickname, password);
                        break;
                    case 7:
                        user.SignOut();
                        break;
                    default:
                        Console.WriteLine("You have entered wrond number.");
                        temp1 = false;
                        Console.Clear();
                        break;
                }
            }
        }

        public void AdministratorInterraction()
        {
            Console.WriteLine("Hello, as a administrator you can do a few things:\n" +
                                "1. Show all products.\n" +
                                "2. Search product by its name.\n" +
                                "3. Create new order and ordering or declining order.\n" +
                                "4. Watch and change user's personal information.\n" +
                                "5. Add new product.\n" +
                                "6. Change information about product.\n" +
                                "7. Change order status.\n" +
                                "8. Sign out.");
            Console.WriteLine("Enter you nickname:");
            string newNickname = Console.ReadLine();
            Console.WriteLine("Enter your password:");
            string newPassword = Console.ReadLine();
            AdministratorAction administrator = new AdministratorAction(newNickname, newPassword);
            bool temp1 = true;
            while (temp1)
            {
                int.TryParse(Console.ReadLine(), out int administratorNumber);
                switch (administratorNumber)
                {
                    case 1:
                        administrator.ShowProducts();
                        break;
                    case 2:
                        Console.WriteLine("Enter product name, you want to search.");
                        string productToSearch = Console.ReadLine();
                        administrator.FindProductByName(productToSearch);
                        break;
                    case 3:
                        administrator.ShowProducts();
                        Console.WriteLine("Enter product number you want to buy.");
                        int.TryParse(Console.ReadLine(), out int numberToBuy);
                        Console.WriteLine("Do you want to confirm your order? \n Yes - 1, No - 2");
                        int.TryParse(Console.ReadLine(), out int choose);
                        administrator.NewOrder(numberToBuy, choose);
                        break;
                    case 4:
                        Console.WriteLine("Enter person's name, you want to change");
                        string name = Console.ReadLine();
                        administrator.ChangeUsersData(name);
                        break;
                    case 5:
                        administrator.ShowProducts();
                        Console.WriteLine("Enter product name:");
                        string productName = Console.ReadLine();
                        Console.WriteLine("Enter product category:");
                        string category = Console.ReadLine();
                        Console.WriteLine("Enter product description:");
                        string description = Console.ReadLine();
                        Console.WriteLine("Enter product price:");
                        int.TryParse(Console.ReadLine(), out int price);
                        administrator.AddProduct(productName, category, description, price);
                        break;
                    case 6:
                        Console.WriteLine("Enter product name:");
                        string nameToSearch = Console.ReadLine();
                        administrator.ChangeProductInformation(nameToSearch);
                        break;
                    case 7:
                        administrator.ShowOrders();
                        Console.WriteLine("Enter person's name.");
                        string input = Console.ReadLine();
                        Console.WriteLine("Choose status");
                        administrator.ShowOrderStatus();
                        Int32.TryParse(Console.ReadLine(), out int num);
                        administrator.ChangeOrderStatus(input, num);
                        break;
                    case 8:
                        administrator.SignOut();
                        break;
                    default:
                        Console.WriteLine("You have entered wrond number.");
                        temp1 = false;
                        Console.Clear();
                        break;
                }
            }
        }
    }
}
