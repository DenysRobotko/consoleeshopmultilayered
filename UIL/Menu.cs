﻿using System;

namespace UIL
{
    public class Menu
    {
        public void MenuInterraction()
        {
            bool temp = true;

            while (temp)
            {
                Console.WriteLine("Press 1 if you are a guest");
                Console.WriteLine("Press 2 if you are a user");
                Console.WriteLine("Press 3 if you are a administrator");
                Console.WriteLine("Press 4 to quit");

                int.TryParse(Console.ReadLine(), out int number);
                switch (number)
                {
                    case 1:
                        Interraction interraction = new Interraction();
                        interraction.GuestInterraction();
                        break;
                    case 2:
                        Interraction userInterraction = new Interraction();
                        userInterraction.UserInterraction();
                        break;
                    case 3:
                        Interraction adminInterraction = new Interraction();
                        adminInterraction.AdministratorInterraction();
                        break;
                    case 4:
                        temp = false;
                        break;
                    default:
                        Console.WriteLine("You have pressed wrong number");
                        break;
                }
            }
        }
    }
}
